'use strict';

/**
 * @ngdoc overview
 * @name awaazApp
 * @description
 * # awaazApp
 *
 * Main module of the application.
 */
angular
  .module('awaazApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/project/:id', {
        templateUrl: 'views/projectDetail.html',
        controller: 'ProjectCtrl',
        controllerAs: 'project'
      })
      .when('/edit/project/:id', {
        templateUrl: 'views/editProject.html',
        controller: 'ProjectCtrl',
        controllerAs: 'eproject'
      })
      .when('/create/project', {
        templateUrl: 'views/createProject.html',
        controller: 'ProjectCtrl',
        controllerAs: 'cproject'
      })
      .when('/project/:id/create/task', {
        templateUrl: 'views/createTask.html',
        controller: 'TaskCtrl',
        controllerAs: 'task'
      })
      .when('/project/:id/edit/task/:id', {
        templateUrl: 'views/editTask.html',
        controller: 'TaskCtrl',
        controllerAs: 'task'
      })
      .when('/404', {
        templateUrl: '404.html'
      })
      .otherwise({
        redirectTo: '/404'
      });
      $locationProvider.html5Mode(true);
  });
