'use strict';

/**
 * @ngdoc function
 * @name awaazApp.controller:TaskCtrl
 * @description
 * # TaskCtrl
 * Controller of the awaazApp
 */
angular.module('awaazApp')
  .controller('TaskCtrl', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    // console.log('Task Ctrl');
    $scope.server_path = "http://52.39.209.17:8003";
    $scope.pid = 0;
    $scope.tid = 0;
    var idstr = window.location.toString();
    $scope.pid = idstr.split("/")[4];
    $scope.tid = (idstr.split("/")[7]) ? idstr.split("/")[7] : 0;
    if($scope.tid != 0){
      var gettask = function() {
        $http({
          method: 'GET',
          url: $scope.server_path+'/task/'+$scope.tid+'/',
          data: {
            format: 'json'
          }
        }).then(function successCallback(data) {
          $scope.tasks = data.data;
        }, function errorCallback(error) {
          console.log('error');
          console.log(error);
        });
      };
      gettask();
    }

    $scope.createTask = function() {
      $http({
        method: 'POST',
        url: $scope.server_path+'/tasks/',
        data: {
          pid: $scope.pid,
          name: $scope.name,
          description: $scope.description,
          start_date: $scope.start_date,
          end_date: $scope.end_date
        }
      }).then(function successCallback(data) {
        $location.path('/project/'+$scope.pid);
        $scope.showProjectForm = false;
      }, function errorCallback(error) {
        console.log(error);
      });
    };

    $scope.editTask = function() {
      $http({
        method: 'PUT',
        url: $scope.server_path+'/task/'+$scope.tid+'/',
        data: {
          id: $scope.tasks.tid,
          pid: $scope.tasks.pid,
          name: $scope.tasks.name,
          description: $scope.tasks.description,
          start_date: $scope.tasks.start_date,
          end_date: $scope.tasks.end_date
        }
      }).then(function successCallback(data) {
        $location.path('/project/'+$scope.tasks.pid);
        $scope.showProjectForm = false;
      }, function errorCallback(error) {
        console.log(error);
      });
    };

  }]);
