'use strict';

/**
 * @ngdoc function
 * @name awaazApp.controller:ProjectCtrl
 * @description
 * # ProjectCtrl
 * Controller of the awaazApp
 */
angular.module('awaazApp')
  .controller('ProjectCtrl', ['$scope', '$http', '$rootScope', '$location', function ($scope, $http, $rootScope, $location) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.pid = 0;
    $scope.server_path = "http://52.39.209.17:8003";
    var idstr = window.location.toString();
    $scope.pid = idstr.split("project/")[1];
    // alert($scope.pid);
    var getsingleProjects = function() {
      $http({
        method: 'GET',
        url: $scope.server_path+'/project/'+$scope.pid+'/',
        data: {
          format: 'json'
        }
      }).then(function successCallback(data) {
        $scope.projects = data.data;
        getptasks();
      }, function errorCallback(error) {
        console.log('error');
        console.log(error);
      });
    };
    var getptasks = function() {
      $http({
        method: 'GET',
        url: $scope.server_path+'/ptask/'+$scope.pid+'/',
        data: {
          format: 'json'
        }
      }).then(function successCallback(data) {
        $scope.projects.tasks = data.data;
      }, function errorCallback(error) {
        console.log('error');
        console.log(error);
      });
    };
    getsingleProjects();

    $scope.createProject = function() {
      $http({
        method: 'POST',
        url: $scope.server_path+'/projects/',
        data: {
          name: $scope.name,
          description: $scope.description,
          avatar: 'test.png',
          duration: $scope.duration
        }
      }).then(function successCallback(data) {
        $location.path('/');
        $scope.showProjectForm = false;
      }, function errorCallback(error) {
        console.log(error);
      });
    };

    $scope.editProject = function() {
      $http({
        method: 'PUT',
        url: $scope.server_path+'/project/'+$scope.projects.id+'/',
        data: {
          name: $scope.projects.name,
          description: $scope.projects.description,
          avatar: 'test.png',
          duration: $scope.projects.duration
        }
      }).then(function successCallback(data) {
        $location.path('/');
        $scope.showProjectForm = false;
      }, function errorCallback(error) {
        console.log(error);
      });
    };

    $scope.deleteTask = function(item) {
      var tid = item.id;
      if(confirm("Are you sure want to delete task "+item.name)){
        $http({
          method: 'DELETE',
          url: $scope.server_path+'/task/'+tid+'/'
        }).then(function successCallback(data) {
          var myInd = $scope.projects.tasks.indexOf(item);
          $scope.projects.tasks.splice(myInd, 1);
        }, function errorCallback(error) {
          console.log(error);
        });
      }
    };
  }]);
