'use strict';

/**
 * @ngdoc function
 * @name awaazApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the awaazApp
 */
angular.module('awaazApp')
  .controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.server_path = "http://52.39.209.17:8003";
    $scope.deleteProject = function(item) {
      var pid = item.id;
      if(confirm("Are you sure want to delete project "+item.name+", if you delete this project then all tasks of this project will also delete")){
        $http({
          method: 'DELETE',
          url: $scope.server_path+'/project/'+pid+'/'
        }).then(function successCallback(data) {
          // delete project's tasks
          $http({
            method: 'DELETE',
            url: $scope.server_path+'/ptask/'+pid+'/'
          }).then(function successCallback(data) {
            console.log("Task deleted");
          }, function errorCallback(error) {
            console.log(error);
          });

          var myInd = $scope.projects.indexOf(item);
          $scope.projects.splice(myInd, 1);
        }, function errorCallback(error) {
          console.log(error);
        });
      }
    };
    
    var getProjects = function() {
      $http({
        method: 'GET',
        url: $scope.server_path+'/projects/',
        data: {
          format: 'json'
        }
      }).then(function successCallback(data) {
        console.log(data);
        $scope.projects = data.data;
      }, function errorCallback(error) {
        console.log('error');
        console.log(error);
      });
    };

    getProjects();

  }]);
