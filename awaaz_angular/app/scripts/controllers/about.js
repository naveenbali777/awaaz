'use strict';

/**
 * @ngdoc function
 * @name awaazApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the awaazApp
 */
angular.module('awaazApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
