# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restapp', '0003_auto_20161216_1015'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='task',
            options={},
        ),
        migrations.AlterUniqueTogether(
            name='task',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='task',
            name='project',
        ),
    ]
