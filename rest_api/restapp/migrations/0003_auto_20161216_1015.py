# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restapp', '0002_task'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='task',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='task',
            name='project',
            field=models.ForeignKey(related_name='task', default=b'0', to='restapp.Project'),
        ),
        migrations.AlterField(
            model_name='task',
            name='pid',
            field=models.IntegerField(),
        ),
        migrations.AlterUniqueTogether(
            name='task',
            unique_together=set([('project', 'name')]),
        ),
    ]
