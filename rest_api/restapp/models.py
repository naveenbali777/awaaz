from django.db import models
from datetime import datetime
# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    avatar = models.CharField(max_length=250)
    duration = models.IntegerField()

class Task(models.Model):
    pid = models.IntegerField()
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)